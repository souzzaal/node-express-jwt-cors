const AuthRequest = require('./requests/AuthRequest')
const AuthController = require('./controllers/AuthController')
const cors = require('cors')
const JwtAuth = require('./middlewares/JwtAuth')
const Request = require('./middlewares/RequestValidator')
const routes = require('express').Router()
const UserController = require('./controllers/UserController')

const GET = 'GET'
const POST = 'POST'
const PUT = 'PUT'
const DELETE = 'DELETE'

routes.use(cors())

/* Public routes */
routes.get('/', (req, res, next) => {
	res.json({ data: { 'api-version': '0.0.1' } })
})

// Auth 
routes.post('/login', AuthRequest.rules(POST), Request.validate, AuthController.login)


/* Private Routes */
routes.use(JwtAuth.verifyJWT)

// Users
routes.get('/users', UserController.index)

module.exports = routes;