'use strict';

const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		name: DataTypes.STRING,
		email: DataTypes.STRING,
		password: {
			type: DataTypes.STRING,
			set(value) {
				this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(10)))
			}
		}
	}, {
		underscored: true,
	});

	User.associate = function (models) {
		// associations can be defined here
	};

	User.prototype.validatePassword = function (password) {
		return bcrypt.compareSync(password, this.password)
	}

	return User;
};