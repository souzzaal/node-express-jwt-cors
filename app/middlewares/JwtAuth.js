const jwt = require('jsonwebtoken')

class JwtAuth {

	verifyJWT = (req, res, next) => {
		const token = req.header('Authorization')

		if (!token) {
			return res.status(401).json({ errors: [{ message: 'Token não informado.' }] })
		}

		jwt.verify(token.replace('Bearer ', ''), process.env.JWT_KEY, (err, decoded) => {
			if (err) {
				return res.status(401).json({ errors: [{ message: 'Token inválido.' }] })
			}

			req.user = decoded;	// Disponibiliza as informações do usuário autenticado (payload jwt) na requsição
			next();
		});
	}
}

module.exports = new JwtAuth()