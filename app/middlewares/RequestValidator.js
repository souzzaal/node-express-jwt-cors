const { body, validationResult } = require('express-validator')

class RequestValidator {

	validate = (req, res, next) => {
		const errors = validationResult(req)
		if (errors.isEmpty()) {
			return next()
		}
		const extractedErrors = []
		errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

		if (process.env.ERROR_LOG_REQUEST == 1) {
			console.log({
				body: req.body,
				query: req.query,
				errors: errors.errors
			});
		}

		return res.status(422).json({
			errors: extractedErrors,
		})
	}
}


module.exports = new RequestValidator()