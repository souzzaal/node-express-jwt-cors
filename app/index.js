require('dotenv-safe').config({
	path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env'
})

const express = require('express')
const routes = require('./routes')

const app = new express()

app.use(express.json())
app.use(routes)

module.exports = app