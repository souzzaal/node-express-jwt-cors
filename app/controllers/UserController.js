const { User } = require('../models')

class UserController {
	async index(req, res) {
		try {
			const users = await User.findAll({ attributes: ['id', 'name', 'email'] })

			return res.json({ data: users })
		} catch (error) {
			return res.status(500).json({ errors: [{ message: error.message }] })
		}
	}
}

module.exports = new UserController()