var jwt = require('jsonwebtoken');

const { User } = require('../models')

class AuthController {
	async login(req, res) {
		try {

			const user = await User.findOne({
				where: {
					email: req.body.data.email
				}
			})

			if (user && user.validatePassword(req.body.data.password)) {

				const payload = {
					sub: user.id,
					name: user.name,
					email: user.email
				}

				const token = jwt.sign(
					payload,
					process.env.JWT_KEY || 'senhaSecretaDaAplicacaoExpressJWT',
					{
						algorithm: "HS256",
						expiresIn: process.env.JWT_EXPIRES_IN || '1h'
					}
				);

				return res.json({
					data: { token }
				})
			} else {
				return res.status(422).json({ errors: [{ message: 'Credenciais inválidas' }] })
			}


		} catch (error) {
			return res.status(500).json({ errors: [{ message: error.message }] })
		}
	}

}

module.exports = new AuthController()