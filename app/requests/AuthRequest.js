const { param, body } = require('express-validator')

class AuthRequest {

	rules = (method) => {

		let rules = null;

		switch (method) {
			case 'POST':
				rules = [
					body('data.email').trim().escape().notEmpty().isEmail(),
					body('data.password').trim().escape().notEmpty()
				]
				break;
		}

		return rules
	}
}

module.exports = new AuthRequest()