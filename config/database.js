require('dotenv-safe').config({
	path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env'
})

module.exports = {
	"host": process.env.DB_HOST,
	"database": process.env.DB_NAME,
	"username": process.env.DB_USER,
	"password": process.env.DB_PASS,
	"dialect": process.env.DB_DIALECT,
	"storage": process.env.DB_STORAGE,
	"logging": process.env.DB_LOGGING || false,    // false: deixa de exibir as queries SQL no terminal
	define: {
		timestamps: true,                          // força a criação dos campos created_at e updated_at
		underscored: true,                         // força que os campos acima sejam criados em snake_case (camelCase é o padrão)
		underscoredAll: true,                      // força todos os campos para snake_case
	}
}