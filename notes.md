# JWT



## O que  é?

Sigla para "JSON Web Token" é um padrão (RFC-7519) que define como transmitir e armazenar objetos JSON de forma compacta e segura entre diferentes aplicações. 



O token é uma string em Base64 que armazena objetos JSON com os dados que permitem a autenticação da requisição. O token é composto por três seções (*Header*, *Payload* e *Signature*) separadas por um ponto, tendo o seguinte formato:



![ ](https://cdn.auth0.com/content/jwt/encoded-jwt3.png)



Ao utilizar o *token*, a aplicação não precisa consultar, a cada requisição, o banco de dados para recuperação das informações do usuário.



![ ](https://static.imasters.com.br/wp-content/uploads/2017/07/02-json.jpg)



## Header



É um objeto JSON que define informações sobre:

-  o tipo (typ) do *token* 

- o algoritmo (alg) de criptografia usado na assinatura
  
  - geralmente HMAC, SH256 ou RSA



Exemplo:

```json
{
    "alg": "SH256",
    "typ": "JWT"
}
```



## Payload



É um  objeto JSON  que contem: 

- as informações (claims) da entidade tratada. 
  
  - Por exemplo, contém as informações (id, nome, e-mail...) do usuário autenticado. 

- o algoritmo (alg) de criptografia usado na assinatura
  
  - geralmente HMAC, SH256 ou RSA



O *payload* pode ser de três tipos:



- *Reserved claims*
  
  - Atributos não obrigatórios (mas recomendados) que são usados na validação do token pelos protocolos de segurança das as APIs:
    
    - sub: (*subject*) Entidade a quem o *token* pertence
      
      - ex: id do usuário
    
    - iss: (*issuer*) Emissor do token
    
    - exp: (*expiration*) Timestamp de expiração do *token*
    
    - iat: (*issued a*t) Timestamp de criação do *token* (pouco usado)
    
    - aud: (*audience*) Destinatário do *token* (pouco usado)
      
      - a aplicação que irá usá-lo

- *Public claims*
  
  - Atributos (do usuário autenticado) usado pelas aplicações 
    
    - nome
    
    - perfil 
    
    - permissões

- *Private claims*
  
  - Atributos definidos especialmente para compartilhar informações entre aplicações



Exemplo:

```json
{
  "sub": "1234567890",
  "name": "John Doe",
  "iat": 1516239022
}
```



## Signature



A assinatura é a concatenação dos hashes gerados a partir do Header e Payload usando base64UrlEncode, com uma chave secreta ou certificado RSA. 



A assinatura garante que a integridade do *token*, pois, apenas quem possui a chave pode criar, alterar e validar o *token*.





## Como pode ser utilizado?



O usuário faz login em um serviço de autenticação, a aplicação valida os dados, gera e retorna um token JWT para ele. Então, em cada requisição, o usuário inclui esse token (prefixado com `Bearer <espaço>`) , no header Authorization. 

 

Forma de uso no HTTP HEADER:

```http
Authorization: Bearer wwwwwwwwww.yyyyyyyyyy.zzzzzzzzzz
```



## Referências

- [JSON Web Token Introduction - jwt.io](https://jwt.io/introduction/)

- [Entendendo JWT](https://medium.com/tableless/entendendo-tokens-jwt-json-web-token-413c6d1397f6)
