# Node Express JWT CORS

Este projeto tem por objetivos (1) estudar o uso de `Json Web Token` como mecanismo de autenticação, e (2) estudar o comparilhamento de recursos via `Cross-origin resource sharing`, ambos para APIs construídas sobre o framework Express.

Tecnologias utilizadas:

- bcrypt
- cors
- dotenv-safe
- express
- express-validator
- factory-girl
- faker
- jest
- jsonwebtoken
- nodemon
- sequelize
- sequelize-cli
- sqlite3
- supertest

Para testar a aplicação instale as dependências

```shell
yarn install
```

e excute os testes

```shell
yarn test
```