const app = require('../../app')
const factory = require('../../database/factories/')
const jwt = require('jsonwebtoken')
const request = require('supertest')

const dataProvider = [
	// desc, dataset
	['sem dados', {}],
	['com email nulo', { email: null, password: '1234' }],
	['com email invalido', { email: 'sadf', password: '1234' }],
	['com password nulo', { email: 'user@mail.com', password: null }],
	['com password vazio', { email: 'user@mail.com', password: '' }],
]

describe('Auth', () => {

	it('deve retornar status 200 ao acessar rota publica', async () => {
		const response = await request(app)
			.get('/')
			.send()

		expect(response.status).toBe(200)
	})

	it.only('deve retornar os dados do usuario criptografados no token JWT ao realizar login com credenciais validas', async () => {
		const user_pw = '12345'
		const user = await factory.create('User', {
			password: user_pw
		})

		const response = await request(app)
			.post('/login')
			.send({
				data: {
					email: user.email,
					password: user_pw
				}
			})

		const data = response.body.data

		tokenPayload = JSON.parse(Buffer.from(data.token.split('.')[1], 'base64').toString('utf8'))

		expect(response.status).toBe(200)
		expect(tokenPayload).toEqual(expect.objectContaining({
			sub: expect.any(Number),		// id do usuário
			name: user.name,
			email: user.email,
			exp: expect.any(Number),
			iat: expect.any(Number)
		}))
	})

	it.each(dataProvider)
		('deve retornar 422 ao realizar login %p', async (desc, inputs) => {
			const response = await request(app)
				.post('/login')
				.send({ data: inputs })

			expect(response.status).toBe(422)
		})

	it('deve retornar 401 ao acessar rota privada sem informar token', async () => {
		const response = await request(app)
			.get('/users')
			.send()

		expect(response.status).toBe(401)
	})

	it('deve retornar 401 ao acessar rota privada com token invalido', async () => {
		const response = await request(app)
			.get('/users')
			.set('Authorization', 'Bearer abc123')
			.send()

		expect(response.status).toBe(401)
	})

	it('deve retornar 401 ao acessar rota privada com token expirado', async () => {

		const user = await factory.create('User')

		const token = jwt.sign(
			{ id: user.id },
			process.env.JWT_KEY,
			{ expiresIn: '0s' }
		);

		const response = await request(app)
			.get('/users')
			.set('Authorization', `Bearer ${token}`)
			.send()

		expect(response.status).toBe(401)
	})

	it('deve retornar 200 ao acessar rota privada com token valido', async () => {

		const user = await factory.create('User')

		const token = jwt.sign(
			{ id: user.id },
			process.env.JWT_KEY,
			{ expiresIn: '60s' }
		);

		const response = await request(app)
			.get('/users')
			.set('Authorization', `Bearer ${token}`)
			.send()


		expect(response.status).toBe(200)
		const data = response.body.data

		expect(data.length).toBeGreaterThanOrEqual(1)

		data.forEach(user => {
			expect(user).toEqual(expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
			}))
		});
	})

})