const bcrypt = require('bcrypt');
const factory = require('../../database/factories/')

describe('User', () => {
	it('deve criptografar senha ao criar usuario', async () => {
		const textPlainPassword = '1234'
		const user = await factory.create('User', {
			password: textPlainPassword
		})

		expect(user.password).not.toBe(textPlainPassword)
		expect(true).toBe(bcrypt.compareSync(textPlainPassword, user.password))
		expect(true).toBe(user.validatePassword(textPlainPassword))
	})

	it('deve criptografar senha ao atualizar usuario', async () => {
		const textPlainPassword = '12345'
		const user = await factory.create('User')
		await user.update({ password: textPlainPassword })

		expect(user.password).not.toBe(textPlainPassword)
		expect(true).toBe(bcrypt.compareSync(textPlainPassword, user.password))
		expect(true).toBe(user.validatePassword(textPlainPassword))
	})
})